package com.arquigames.aprendiendoespanol.items;

import android.app.Activity;
import android.os.Bundle;

import com.arquigames.aprendiendoespanol.R;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arquigames.aprendiendoespanol.items.dummy.DummyContent;

/**
 * A fragment representing a single ItemSaludo detail screen.
 * This fragment is either contained in a {@link ItemElementsListActivity}
 * in two-pane mode (on tablets) or a {@link ItemElementsDetailActivity}
 * on handsets.
 */
public class ItemElementsDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    public static final String ARG_TITLE = "item_title";

    /**
     * The dummy content this fragment is presenting.
     */
    private DummyContent.DummyItem mItem;
    private String mTitle;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemElementsDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            if (getArguments().containsKey(ARG_ITEM_ID)) {
                // Load the dummy content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
                mTitle = getArguments().getString(ARG_TITLE);
                this.getActivity().setTitle(mTitle);
                Activity activity = this.getActivity();
                assert activity != null;
            /*
            CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.content);
            }
            */
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.itemsaludo_detail, container, false);

        // Show the dummy content as text in a TextView.
        /*
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.itemsaludo_detail)).setText(mItem.dummyGroup.resources.get(0).text);
        }
        */

        View recyclerView = rootView.findViewById(R.id.itemsaludo_detail_list);
        assert recyclerView != null;
        ItemElementsDetailActivity.setupRecyclerView((RecyclerView) recyclerView,mItem,this.getActivity());





        return rootView;
    }
}
