package com.arquigames.aprendiendoespanol.items.dummy;

import com.arquigames.aprendiendoespanol.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<DummyItem> ITEMS = new ArrayList<>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, DummyItem> ITEM_MAP = new HashMap<>();

    private static ArrayList<DummyGroup> titles = new ArrayList<>();


    public static void fillSaludos(){
        titles.clear();
        ArrayList<DummyResource> arr = new ArrayList<>();
        arr.add(new DummyResource(R.raw.reuniones_hola,"Hola!"));
        arr.add(new DummyResource(R.raw.reuniones_hola_hijo,"Hola hijo"));
        arr.add(new DummyResource(R.raw.reuniones_hola_hija,"Hola hija"));
        arr.add(new DummyResource(R.raw.reuniones_hola_papa,"Hola papá"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mama,"Hola mamá"));
        arr.add(new DummyResource(R.raw.reuniones_hola_hermano,"Hola hermano"));
        arr.add(new DummyResource(R.raw.reuniones_hola_hermana,"Hola hermana"));
        arr.add(new DummyResource(R.raw.reuniones_hola_tio,"Hola tío"));
        arr.add(new DummyResource(R.raw.reuniones_hola_tia,"Hola tía"));
        arr.add(new DummyResource(R.raw.reuniones_hola_sobrino,"Hola sobrino"));
        arr.add(new DummyResource(R.raw.reuniones_hola_sobrina,"Hola sobrina"));
        arr.add(new DummyResource(R.raw.reuniones_hola_abuela,"Hola abuela"));
        arr.add(new DummyResource(R.raw.reuniones_hola_abuelo,"Hola abuelo"));
        arr.add(new DummyResource(R.raw.reuniones_hola_tia_abuela,"Hola tía abuela"));
        arr.add(new DummyResource(R.raw.reuniones_hola_tio_abuelo,"Hola tío abuelo"));
        arr.add(new DummyResource(R.raw.reuniones_hola_bisabuela,"Hola bisabuela"));
        arr.add(new DummyResource(R.raw.reuniones_hola_bisabuelo,"Hola bisabuelo"));
        arr.add(new DummyResource(R.raw.reuniones_hola_tatarabuelo,"Hola tatarabuela"));
        arr.add(new DummyResource(R.raw.reuniones_hola_tatarabuelo,"Hola tatarabuelo"));
        arr.add(new DummyResource(R.raw.reuniones_hola_primo,"Hola primo"));
        arr.add(new DummyResource(R.raw.reuniones_hola_prima,"Hola prima"));
        arr.add(new DummyResource(R.raw.reuniones_hola_nieto,"Hola nieto"));
        arr.add(new DummyResource(R.raw.reuniones_hola_nieta,"Hola nieta"));
        arr.add(new DummyResource(R.raw.reuniones_hola_bisnieto,"Hola bisnieto"));
        arr.add(new DummyResource(R.raw.reuniones_hola_bisnieta,"Hola bisnieta"));
        arr.add(new DummyResource(R.raw.reuniones_hola_padrino,"Hola padrino"));
        arr.add(new DummyResource(R.raw.reuniones_hola_madrina,"Hola madrina"));
        arr.add(new DummyResource(R.raw.reuniones_hola_ahijado,"Hola ahijado"));
        arr.add(new DummyResource(R.raw.reuniones_hola_ahijada,"Hola ahijada"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_querido_nieto,"Hola mi querido nieto"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_querida_nieta,"Hola mi querido nieta"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_querido_sobrino,"Hola mi querido sobrino"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_querida_sobrina,"Hola mi querido sobrina"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_querida,"Hola mi querida"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_querido,"Hola mi querido"));
        arr.add(new DummyResource(R.raw.reuniones_hola_amor,"Hola amor"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_vida,"Hola mi vida"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_cielo,"Hola mi cielo"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_rey,"Hola mi rey"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_reina,"Hola mi reina"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_principe,"Hola mi principe"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_princesa,"Hola mi princesa"));
        arr.add(new DummyResource(R.raw.reuniones_hola_vecino,"Hola vecino"));
        arr.add(new DummyResource(R.raw.reuniones_hola_vecina,"Hola vecina"));
        arr.add(new DummyResource(R.raw.reuniones_hola_profesor,"Hola profesor"));
        arr.add(new DummyResource(R.raw.reuniones_hola_profesora,"Hola profesora"));
        arr.add(new DummyResource(R.raw.reuniones_hola_tutor,"Hola tutor"));
        arr.add(new DummyResource(R.raw.reuniones_hola_tutora,"Hola tutora"));
        arr.add(new DummyResource(R.raw.reuniones_hola_maestro,"Hola maestro"));
        arr.add(new DummyResource(R.raw.reuniones_hola_maestra,"Hola maestra"));
        arr.add(new DummyResource(R.raw.reuniones_hola_amigo,"Hola amigo"));
        arr.add(new DummyResource(R.raw.reuniones_hola_amiga,"Hola amiga"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_estimado,"Hola mi estimado"));
        arr.add(new DummyResource(R.raw.reuniones_hola_mi_estimada,"Hola mi estimada"));
        arr.add(new DummyResource(R.raw.reuniones_hola_congresista,"Hola congresista"));
        arr.add(new DummyResource(R.raw.reuniones_hola_periodista,"Hola periodista"));
        arr.add(new DummyResource(R.raw.reuniones_hola_firulay,"Hola firulay"));
        arr.add(new DummyResource(R.raw.reuniones_hola_reportera,"Hola reportera"));
        arr.add(new DummyResource(R.raw.reuniones_hola_reportero,"Hola reportero"));
        arr.add(new DummyResource(R.raw.reuniones_hola_actor,"Hola actor"));
        arr.add(new DummyResource(R.raw.reuniones_hola_actriz,"Hola actriz"));
        arr.add(new DummyResource(R.raw.reuniones_hola_ingeniero,"Hola ingeniero"));
        arr.add(new DummyResource(R.raw.reuniones_hola_ingeniera,"Hola ingeniera"));
        arr.add(new DummyResource(R.raw.reuniones_hola_carpintero,"Hola carpintero"));
        arr.add(new DummyResource(R.raw.reuniones_hola_carpintera,"Hola carpintera"));
        arr.add(new DummyResource(R.raw.reuniones_hola_abogado,"Hola abogado"));
        arr.add(new DummyResource(R.raw.reuniones_hola_abogada,"Hola abogada"));
        arr.add(new DummyResource(R.raw.reuniones_hola_programador,"Hola programador"));
        arr.add(new DummyResource(R.raw.reuniones_hola_programadora,"Hola programadora"));
        arr.add(new DummyResource(R.raw.reuniones_hola_arquitecto,"Hola arquitecto"));
        arr.add(new DummyResource(R.raw.reuniones_hola_arquitecta,"Hola arquitecta"));
        arr.add(new DummyResource(R.raw.reuniones_hola_dentista,"Hola dentista"));
        arr.add(new DummyResource(R.raw.reuniones_hola_policia,"Hola policía"));
        arr.add(new DummyResource(R.raw.reuniones_hola_enfermero,"Hola enfermero"));
        arr.add(new DummyResource(R.raw.reuniones_hola_enfermera,"Hola enfermera"));
        arr.add(new DummyResource(R.raw.reuniones_hola_medico,"Hola médico"));
        arr.add(new DummyResource(R.raw.reuniones_hola_auxiliar,"Hola auxiliar"));
        arr.add(new DummyResource(R.raw.reuniones_hola_secretaria,"Hola secretaria"));
        arr.add(new DummyResource(R.raw.reuniones_hola_vendedor,"Hola vendedor"));
        arr.add(new DummyResource(R.raw.reuniones_hola_vendedora,"Hola vendedora"));
        arr.add(new DummyResource(R.raw.reuniones_hola_surfista,"Hola surfista"));
        arr.add(new DummyResource(R.raw.reuniones_hola_contador,"Hola contador"));
        arr.add(new DummyResource(R.raw.reuniones_hola_contadora,"Hola contadora"));
        arr.add(new DummyResource(R.raw.reuniones_hola_visitador_medico,"Hola visitador médico"));
        arr.add(new DummyResource(R.raw.reuniones_hola_visitadora_medica,"Hola visitadora médica"));
        titles.add(new DummyGroup("¡Hola!",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(R.raw.reuniones_como_estas1,"¿ Cómo estás?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas2,"¿ Cómo estás eliana ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas3,"¿ Cómo estás estimado amigo ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas4,"¿ Cómo estás estimada amiga ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas5,"¿ Cómo has estado ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas6,"¿ Cómo has estado ? ¿ Desde cuándo no nos vemos ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas7,"¿ Cómo has estado ? ¿ Cuánto tiempo sin vernos ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas8,"¿ Cómo has estado ? ¡ Cuánto tiempo sin vernos !"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas9,"¿ Qué tal amiga ? ¿ Cómo estás ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas10,"¿ Qué tal amigo ? ¿ Cómo estás ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas11,"¿ Cómo te fué ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas12,"¿ Y qué tal ? ¿ cómo te fué ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas13,"¿ Cómo te fué en tu presentación ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas14,"¿ Cómo te fué en tu presentación teatral ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas15,"¿ Cómo te fué en la entrevista de trabajo ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas16,"¿ Cómo te fué con tu novio ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas17,"¿ Cómo te fué con tu novia ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas18,"¿ Cómo te fué con tu pareja ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas20,"¿ Cómo te fué con tu reconciliación con tus padres ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas19,"¿ Cómo te fué con tu reconciliación con tu pareja ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas21,"¿ Cómo te fué en la reconciliación con tus padres ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas22,"¿ Cómo te fué en la reconciliación con tu pareja ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas23,"¿ Cómo te fué en la tarea de la escuela ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas24,"¿ Cómo te fué con la tarea de la escuela ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas25,"¿ Cómo te fué en la tarea del colegio ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas26,"¿ Cómo te fué con la tarea del colegio ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas27,"¿ Cómo te fué en los ejercicios matemáticos ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas28,"¿ Cómo te fué con los ejercicios matemáticos ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas29,"¿ Cómo te fué con tu jefe ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas30,"¿ Cómo te fué con tu jefa ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas31,"¿ Cómo te fué en el taller de artes escénicas ?"));
        arr.add(new DummyResource(R.raw.reuniones_como_estas32,"¿ Cómo te fué en el viaje a Machu Picchu ?"));
        titles.add(new DummyGroup("¿ Cómo estás ?",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(R.raw.reuniones_chau1,"Chau, nos vemos"));
        arr.add(new DummyResource(R.raw.reuniones_chau2,"Gracias por ayudarme, chau"));
        arr.add(new DummyResource(R.raw.reuniones_chau3,"Gracias por ayudarme, ¡ nos vemos pronto !"));
        arr.add(new DummyResource(R.raw.reuniones_chau4,"Gracias por ayudarme, ¡ estamos conversando !"));
        arr.add(new DummyResource(R.raw.reuniones_chau5,"Bueno, nos vemos en la reunión"));
        arr.add(new DummyResource(R.raw.reuniones_chau6,"Bueno, nos vemos en el colegio"));
        arr.add(new DummyResource(R.raw.reuniones_chau7,"Bueno, nos vemos en la universidad"));
        arr.add(new DummyResource(R.raw.reuniones_chau8,"Bueno, nos vemos en el trabajo"));
        arr.add(new DummyResource(R.raw.reuniones_chau9,"Gracias por tu ayuda"));
        titles.add(new DummyGroup("Chau, nos vemos",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Gusto en conocerte"));
        titles.add(new DummyGroup("Gusto en conocerte",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"! Qué te vaya bien !"));
        arr.add(new DummyResource(0,"! Qué te vaya bien en tu viaje !"));
        arr.add(new DummyResource(0,"Gracias por ayudarme ! Qué te vaya bien !"));
        arr.add(new DummyResource(0,"Gracias por tu visita amiga ! Qué te vaya bien !"));
        arr.add(new DummyResource(0,"Gracias por su visita querida familia ! Qué les vaya bien en su retorno !"));
        titles.add(new DummyGroup("!Qué te vaya bien!",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Buen día"));
        arr.add(new DummyResource(0,"Buenos días"));
        arr.add(new DummyResource(0,"Buenos días papis"));
        arr.add(new DummyResource(0,"Buenos días papito"));
        arr.add(new DummyResource(0,"Buenos días mamá"));
        arr.add(new DummyResource(0,"Buenos días papá"));
        arr.add(new DummyResource(0,"Buenos días familia"));
        arr.add(new DummyResource(0,"Buenos días a todo el mundo"));
        arr.add(new DummyResource(0,"Buenos días a todos los presentes"));
        arr.add(new DummyResource(0,"Buenas noches eliana"));
        arr.add(new DummyResource(0,"Buenas noches hijo"));
        arr.add(new DummyResource(0,"Buenas noches, descansen"));
        arr.add(new DummyResource(0,"Buenas tardes amigos"));
        arr.add(new DummyResource(0,"Buenas tardes mi amor"));
        arr.add(new DummyResource(0,"Buenas noches ¿ cómo te fué durante el día ?"));
        arr.add(new DummyResource(0,"¿ Cómo has amanecido el día de hoy ?"));
        arr.add(new DummyResource(0,"¿ Cómo has amanecido hoy ?"));
        arr.add(new DummyResource(0,"¿ Cómo has amanecido hoy día ?"));
        titles.add(new DummyGroup("Buen día",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"¡Genial!"));
        arr.add(new DummyResource(0,"¡Eso suenta espectacular!"));
        arr.add(new DummyResource(0,"¡Lo que acabas de mencionar suena bueno!"));
        arr.add(new DummyResource(0,"¡Lo que acabas de mencionar suena maravilloso!"));
        arr.add(new DummyResource(0,"¡Lo que acabas de mencionar suena padrísimo!"));
        arr.add(new DummyResource(0,"¡Lo que acabas de mencionar suena espectacular!"));
        arr.add(new DummyResource(0,"¡Acaban de alegrarme el día!"));
        arr.add(new DummyResource(0,"¡Es grandioso!"));
        arr.add(new DummyResource(0,"¡Sueña de maravilla!"));
        arr.add(new DummyResource(0,"¡Wow... suena increíble!"));
        arr.add(new DummyResource(0,"¡Wow... Felicitaciones por tu logro!"));
        arr.add(new DummyResource(0,"¡Es un hecho impresionante!"));
        arr.add(new DummyResource(0,"¡Genial, hagámoslo!"));
        titles.add(new DummyGroup("!Genial!",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Más o menos"));
        arr.add(new DummyResource(0,"Más o menos te entiendo"));
        arr.add(new DummyResource(0,"Tu tarea está más o menos aprobada"));
        arr.add(new DummyResource(0,"Tu tarea está más o menos entendible"));
        arr.add(new DummyResource(0,"Tu tarea está entendible a medias"));
        arr.add(new DummyResource(0,"Tu tarea se entiende a las justas"));
        titles.add(new DummyGroup("Más o menos",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"¿ Cómo te llamas ?"));
        arr.add(new DummyResource(0,"¿ Cómo dijiste que te llamabas ?"));
        arr.add(new DummyResource(0,"¿ Porfavor, me puedes repetir tu nombre ?"));
        arr.add(new DummyResource(0,"Porfavor, escribe aquí tu nombre"));
        arr.add(new DummyResource(0,"Porfavor, escribe allí tu nombre"));
        arr.add(new DummyResource(0,"Porfavor, escribe ahí tu nombre"));
        arr.add(new DummyResource(0,"¿ Cuál era tu nombre ?"));
        arr.add(new DummyResource(0,"¿ Me repites tu nombre ?"));
        arr.add(new DummyResource(0,"¿ Quién fué que preguntó cómo te llamas ?"));
        arr.add(new DummyResource(0,"¿ Cómo me llamo ?"));
        arr.add(new DummyResource(0,"¿ Cómo dijiste que me llamaba ?"));
        titles.add(new DummyGroup("¿ Cómo te llamas ?",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Ten cuidado"));
        arr.add(new DummyResource(0,"Ten cuidado porfavor"));
        arr.add(new DummyResource(0,"Ten cuidado porfavor, todo puede suceder cuando vayas a ese sitio"));
        arr.add(new DummyResource(0,"Ten cuidado porfavor, mira a todos lados"));
        arr.add(new DummyResource(0,"Ten cuidado porfavor, primero pregunta a donde quieres ir"));
        arr.add(new DummyResource(0,"Ten cuidado al tomar cualquier decisión"));
        arr.add(new DummyResource(0,"Ten cuidado al tomar cualquier decisión en tu vida"));
        arr.add(new DummyResource(0,"Ten cuidado con responder mal las preguntas"));
        titles.add(new DummyGroup("Ten cuidado",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Hace tiempo"));
        arr.add(new DummyResource(0,"Hace tiempo que extraño la comida peruana"));
        arr.add(new DummyResource(0,"Desde que estoy aquí extraño mucho a mi familia"));
        arr.add(new DummyResource(0,"Hace tiempo viví una experiencia única"));
        arr.add(new DummyResource(0,"Hace tiempo viví una experiencia increíble"));
        arr.add(new DummyResource(0,"Hace tiempo que ya no recuerdo esos pasos"));
        arr.add(new DummyResource(0,"Hace tiempo que ya no me acuerdo como eran esos pasos"));
        arr.add(new DummyResource(0,"Hace tiempo me di cuenta de eso"));
        arr.add(new DummyResource(0,"Hace tiempo me di cuenta de aquello"));
        arr.add(new DummyResource(0,"Hace tiempo me di cuenta de ello"));
        arr.add(new DummyResource(0,"Hace mucho que ya no viene a casa"));
        arr.add(new DummyResource(0,"Hace mucho que ya no viene a visitarme"));
        arr.add(new DummyResource(0,"Hace mucho que ya no trabajo en esa empresa"));
        arr.add(new DummyResource(0,"Hace mucho que ya no vivo en ese lugar"));
        arr.add(new DummyResource(0,"Hace mucho que ya no vivo en ese sitio"));
        arr.add(new DummyResource(0,"Hace mucho nos separamos"));
        arr.add(new DummyResource(0,"Hace mucho ya no es lo mismo"));
        titles.add(new DummyGroup("Hace tiempo",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"¿ Puedo hablar ?"));
        arr.add(new DummyResource(0,"Puedo hablar español"));
        arr.add(new DummyResource(0,"Puedo hablar inglés"));
        arr.add(new DummyResource(0,"Puedo hablar francés"));
        arr.add(new DummyResource(0,"Puedo hablar alemán"));
        arr.add(new DummyResource(0,"Puedo hablar ruso"));
        arr.add(new DummyResource(0,"Puedo hablar chino"));
        arr.add(new DummyResource(0,"Puedo hablar aymara"));
        arr.add(new DummyResource(0,"Puedo hablar quechua"));
        arr.add(new DummyResource(0,"Puedo hablar catalán"));
        arr.add(new DummyResource(0,"No puedo hablar bien"));
        arr.add(new DummyResource(0,"No me acuerdo cómo hablar ese idioma"));
        arr.add(new DummyResource(0,"¿ Puedo hablarte ?"));
        arr.add(new DummyResource(0,"¿ Puedo hablarle ?"));
        arr.add(new DummyResource(0,"¿ Tú crees que yo pueda hablarle ?"));
        arr.add(new DummyResource(0,"¿ Tú crees que puedo hablarte ?"));
        arr.add(new DummyResource(0,"¿ Tú crees que puedo hablarte ahora mismo sobre ese tema ?"));
        arr.add(new DummyResource(0,"¿ Tú crees que puedo hablarte ahora mismo sobre ese detalle ?"));
        arr.add(new DummyResource(0,"¿ Tú crees que puedo hablarte ahora mismo sobre ello ?"));
        arr.add(new DummyResource(0,"¿ Tú crees que puedo hablarte sobre lo que te comenté hoy por la mañana ?"));
        arr.add(new DummyResource(0,"¿ Tú crees que puedo hablarte sobre lo que te comenté hoy por la tarde ?"));
        arr.add(new DummyResource(0,"¿ Tú crees que puedo hablarte sobre lo que te comenté hoy por la noche ?"));
        arr.add(new DummyResource(0,"¿ Tú crees que pueda hablar bien en público ?"));
        arr.add(new DummyResource(0,"¿ Tú crees que pueda hablarle sin miedo ?"));
        arr.add(new DummyResource(0,"¿ Podemos hablarle al respecto ?"));
        arr.add(new DummyResource(0,"¿ Podemos hablarle en un par de horas ?"));
        arr.add(new DummyResource(0,"¿ Podemos hablarle en un unos cuántos minutos ?"));
        arr.add(new DummyResource(0,"¿ Podemos conversar un poco del tema ?"));
        arr.add(new DummyResource(0,"¿ Podemos conversar en las afueras de la escuela ?"));
        arr.add(new DummyResource(0,"¿ Podemos conversar fuera del trabajo ?"));
        arr.add(new DummyResource(0,"¿ Podemos conversar fuera del horario del trabajo ?"));
        titles.add(new DummyGroup("Puedo hablar...",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Tengo tantos años"));
        arr.add(new DummyResource(0,"Tengo tantos años como la edad que tiene eliana"));
        arr.add(new DummyResource(0,"Tengo la misma edad de eliana"));
        arr.add(new DummyResource(0,"Tengo tantos años como los años que lleva casada mi tía"));
        arr.add(new DummyResource(0,"Tengo la mitad de la edad de mi papá"));
        arr.add(new DummyResource(0,"Mi edad es la mitad de la edad de mi papá"));
        titles.add(new DummyGroup("Tengo tantos años",arr));

        DummyContent.fill();
    }

    public static void fillNumeros(){
        titles.clear();
        ArrayList<DummyResource> arr = new ArrayList<>();
        arr.add(new DummyResource(0,"1. Uno"));
        arr.add(new DummyResource(0,"2. Dos"));
        arr.add(new DummyResource(0,"3. Tres"));
        arr.add(new DummyResource(0,"4. Cuatro"));
        arr.add(new DummyResource(0,"5. Cinco"));
        arr.add(new DummyResource(0,"6. Seis"));
        arr.add(new DummyResource(0,"7. Siete"));
        arr.add(new DummyResource(0,"8. Ocho"));
        arr.add(new DummyResource(0,"9. Nueve"));
        arr.add(new DummyResource(0,"10. Diez"));
        arr.add(new DummyResource(0,"11. Once"));
        arr.add(new DummyResource(0,"12. Doce"));
        arr.add(new DummyResource(0,"13. Trece"));
        arr.add(new DummyResource(0,"14. Catorce"));
        arr.add(new DummyResource(0,"15. Quince"));
        arr.add(new DummyResource(0,"16. Dieciséis"));
        arr.add(new DummyResource(0,"17. Diecisiete"));
        arr.add(new DummyResource(0,"18. Dieciocho"));
        arr.add(new DummyResource(0,"19. Diecinueve"));
        arr.add(new DummyResource(0,"20. Veinte"));
        arr.add(new DummyResource(0,"21. Veintiuno"));
        arr.add(new DummyResource(0,"22. Veintidos"));
        arr.add(new DummyResource(0,"23. Veintitres"));
        arr.add(new DummyResource(0,"24. Veinticuatro"));
        arr.add(new DummyResource(0,"25. Veinticinco"));
        arr.add(new DummyResource(0,"26. Veintiséis"));
        arr.add(new DummyResource(0,"27. Veintisiete"));
        arr.add(new DummyResource(0,"28. Veintiocho"));
        arr.add(new DummyResource(0,"29. Veintinueve"));
        arr.add(new DummyResource(0,"30. Treinta"));
        arr.add(new DummyResource(0,"31. Treinta y uno"));
        arr.add(new DummyResource(0,"32. Treinta y dos"));
        arr.add(new DummyResource(0,"33. Treinta y tres"));
        arr.add(new DummyResource(0,"40. Cuarenta"));
        arr.add(new DummyResource(0,"41. Cuarenta y uno"));
        arr.add(new DummyResource(0,"42. Cuarenta y dos"));
        arr.add(new DummyResource(0,"43. Cuarenta y tres"));
        arr.add(new DummyResource(0,"50. Cincuenta"));
        arr.add(new DummyResource(0,"51. Cincuenta y uno"));
        arr.add(new DummyResource(0,"60. Sesenta"));
        arr.add(new DummyResource(0,"61. Sesenta y uno"));
        arr.add(new DummyResource(0,"70. Setenta"));
        arr.add(new DummyResource(0,"71. Setenta y uno"));
        arr.add(new DummyResource(0,"80. Ochenta"));
        arr.add(new DummyResource(0,"81. Ochenta y uno"));
        arr.add(new DummyResource(0,"90. Noventa"));
        arr.add(new DummyResource(0,"91. Noventa y uno"));
        arr.add(new DummyResource(0,"100. Cien"));
        arr.add(new DummyResource(0,"101. Ciento uno"));
        arr.add(new DummyResource(0,"102. Ciento dos"));
        arr.add(new DummyResource(0,"103. Ciento tres"));
        arr.add(new DummyResource(0,"110. Ciento diez"));
        arr.add(new DummyResource(0,"111. Ciento once"));
        arr.add(new DummyResource(0,"120. Ciento veinte"));
        arr.add(new DummyResource(0,"130. Ciento treinta"));
        arr.add(new DummyResource(0,"131. Ciento treinta y uno"));
        arr.add(new DummyResource(0,"199. Ciento noventa y nueve"));
        arr.add(new DummyResource(0,"200. Doscientos"));
        arr.add(new DummyResource(0,"201. Doscientos uno"));
        arr.add(new DummyResource(0,"202. Doscientos dos"));
        arr.add(new DummyResource(0,"210. Doscientos diez"));
        arr.add(new DummyResource(0,"220. Doscientos veinte"));
        arr.add(new DummyResource(0,"300. Trescientos"));
        arr.add(new DummyResource(0,"400. Cuatrocientos"));
        arr.add(new DummyResource(0,"500. Quinientos"));
        arr.add(new DummyResource(0,"600. Seiscientos"));
        arr.add(new DummyResource(0,"700. Setecientos"));
        arr.add(new DummyResource(0,"800. Ochocientos"));
        arr.add(new DummyResource(0,"900. Novecientos"));
        arr.add(new DummyResource(0,"1000. Mil"));
        arr.add(new DummyResource(0,"1001. Mil uno"));
        arr.add(new DummyResource(0,"1002. Mil dos"));
        arr.add(new DummyResource(0,"1003. Mil tres"));
        arr.add(new DummyResource(0,"1234. Mil doscientos treinta y cuatro"));
        arr.add(new DummyResource(0,"1583. Mil quinientos ochenta y tres"));
        arr.add(new DummyResource(0,"10000. Diez mil"));
        arr.add(new DummyResource(0,"10001. Diez mil uno"));
        arr.add(new DummyResource(0,"10200. Diez mil doscientos"));
        arr.add(new DummyResource(0,"12345. Doce mil trescientos cuarenta y cinco"));
        arr.add(new DummyResource(0,"100000. Cien mil"));
        arr.add(new DummyResource(0,"100001. Cien mil uno"));
        arr.add(new DummyResource(0,"100002. Cien mil dos"));
        arr.add(new DummyResource(0,"111111. Ciento once mil ciento once"));
        arr.add(new DummyResource(0,"123456. Ciento veintitres mil cuatrocientos cincuenta y seis"));
        arr.add(new DummyResource(0,"148973. Ciento cuarenta y ocho mil novecientos setenta y tres"));
        arr.add(new DummyResource(0,"1000000. Un millón"));
        arr.add(new DummyResource(0,"2000000. Dos millones"));
        arr.add(new DummyResource(0,"1 000 000 000 000. Un billón"));
        arr.add(new DummyResource(0,"2 000 000 000 000. Dos billones"));
        arr.add(new DummyResource(0,"3 000 000 000 000. Tres billones"));
        titles.add(new DummyGroup("Los números",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"1ro,1° Primer, Primero(a)"));
        arr.add(new DummyResource(0,"2do,2° Segundo(a)"));
        arr.add(new DummyResource(0,"3ro,3° Tercer, Tercero(a)"));
        arr.add(new DummyResource(0,"4to,4° Cuarto(a)"));
        arr.add(new DummyResource(0,"5to,5° Quinto(a)"));
        arr.add(new DummyResource(0,"6to,6° Sexto(a)"));
        arr.add(new DummyResource(0,"7mo,7° Séptimo(a)"));
        arr.add(new DummyResource(0,"8vo,8° Octavo(a)"));
        arr.add(new DummyResource(0,"9no,9° Noveno(a)"));
        arr.add(new DummyResource(0,"10mo,10° Décimo(a)"));
        arr.add(new DummyResource(0,"11mo,11° Undécimo(a)"));
        arr.add(new DummyResource(0,"12mo,12° Duodécimo(a)"));
        arr.add(new DummyResource(0,"13ro,13° Decimotercero(a)"));
        arr.add(new DummyResource(0,"14to,14° Decimocuarto(a)"));
        arr.add(new DummyResource(0,"15° Decimoquinto(a)"));
        arr.add(new DummyResource(0,"20° Vigésimo(a)"));
        arr.add(new DummyResource(0,"21° Vigésimoprimero(a)"));
        arr.add(new DummyResource(0,"22° Vigésimosegundo(a)"));
        arr.add(new DummyResource(0,"30° Trigésimo(a)"));
        arr.add(new DummyResource(0,"31° Trigésimoprimero(a)"));
        arr.add(new DummyResource(0,"40° Cuadragésimo(a)"));
        arr.add(new DummyResource(0,"50° Quincuagésimo(a)"));
        arr.add(new DummyResource(0,"60° Sexagésimo(a)"));
        arr.add(new DummyResource(0,"70° Septuagésimo(a)"));
        arr.add(new DummyResource(0,"80° Octogésimo(a)"));
        arr.add(new DummyResource(0,"90° Nonagésimo(a)"));
        arr.add(new DummyResource(0,"100° Centésimo(a)"));
        arr.add(new DummyResource(0,"200° Ducentésimo(a)"));
        arr.add(new DummyResource(0,"300° Tricentésimo(a)"));
        arr.add(new DummyResource(0,"400° Cuadringentésimo(a)"));
        arr.add(new DummyResource(0,"500° Quingentésimo(a)"));
        arr.add(new DummyResource(0,"600° Sexcentésimo(a)"));
        arr.add(new DummyResource(0,"700° Septingentésimo(a)"));
        arr.add(new DummyResource(0,"800° Octingentésimo(a)"));
        arr.add(new DummyResource(0,"900° Noningentésimo(a)"));
        arr.add(new DummyResource(0,"1000° Milésimo(a)"));
        arr.add(new DummyResource(0,"2000° Dosmilésimo(a)"));
        arr.add(new DummyResource(0,"Bienvenidos al duodécimo aniversario de nuestra institución educativa"));
        arr.add(new DummyResource(0,"Soy el primer puesto de mi clase"));
        arr.add(new DummyResource(0,"En el examen de admisión ocupé el tercer puesto"));
        arr.add(new DummyResource(0,"El atleta ocupó el primer lugar"));
        titles.add(new DummyGroup("Números ordinales",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Uno por tres es igual a tres"));
        arr.add(new DummyResource(0,"Dos por tres es igual a seis"));
        arr.add(new DummyResource(0,"La multiplicación de dos por tres es igual a seis"));
        arr.add(new DummyResource(0,"La raíz cuadrada de cuatro es dos"));
        arr.add(new DummyResource(0,"La raíz cuadrada de nueve es tres"));
        arr.add(new DummyResource(0,"La raíz cúbica de veintisiete es tres"));
        arr.add(new DummyResource(0,"Cuatro elevado al cuadrado es dieciséis"));
        arr.add(new DummyResource(0,"Cinco elevado al cubo es ciento veinticinco"));
        arr.add(new DummyResource(0,"Cién entre dos me da cincuenta"));
        arr.add(new DummyResource(0,"Cién entre dos resulta cincuenta"));
        arr.add(new DummyResource(0,"La división entre cién y dos resulta cincuenta"));
        arr.add(new DummyResource(0,"Si divido cién entre dos me da o resulta ser cincuenta"));
        arr.add(new DummyResource(0,"Cinco menos tres es igual a dos"));
        arr.add(new DummyResource(0,"Cinco mas tres es igual a ocho"));
        arr.add(new DummyResource(0,"Necesito cinco más de éstas frutas"));
        arr.add(new DummyResource(0,"Tengo cinco moscas vivas y si mato dos moscas, entonces me quedan tres vivas"));
        arr.add(new DummyResource(0,"Compro siete dólares de comida con un billete de diez dólares y me dan de vuelto tres dólares"));
        arr.add(new DummyResource(0,"Compro siete dólares de comida con un billete de diez dólares y me dan tres dólares de vuelto"));
        arr.add(new DummyResource(0,"Compro dos dólares de hisopos con un billete de veinte dólares y me dan de vuelto dieciocho dólares"));
        arr.add(new DummyResource(0,"Compró dos dólares de hisopos y lo estafaron entregándole uno"));
        arr.add(new DummyResource(0,"Erick tiene cuarenta y cinco dólares"));
        arr.add(new DummyResource(0,"Erick tiene trece años de edad"));
        arr.add(new DummyResource(0,"Mi mamá compró ciento cincuenta y seis caramelos"));
        arr.add(new DummyResource(0,"En mi trabajo me pagan tres mil dólares mensuales"));
        arr.add(new DummyResource(0,"Yo cobro tres mil dólares mensuales"));
        arr.add(new DummyResource(0,"Mi edad es cinco veces mayor al tuyo"));
        arr.add(new DummyResource(0,"Mi edad es igual a cinco veces el tuyo"));
        arr.add(new DummyResource(0,"Hoy comí dos amburguesas de pollo"));
        arr.add(new DummyResource(0,"Hoy comí una amburguesa de carne"));
        titles.add(new DummyGroup("Matemáticas",arr));

        DummyContent.fill();
    }
    public static void fillTransporte() {
        titles.clear();
        ArrayList<DummyResource> arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Medios de ransporte"));
        arr.add(new DummyResource(0,"Auto"));
        arr.add(new DummyResource(0,"Carro"));
        arr.add(new DummyResource(0,"Bus"));
        arr.add(new DummyResource(0,"Avión"));
        arr.add(new DummyResource(0,"Avioneta"));
        arr.add(new DummyResource(0,"Tanque"));
        arr.add(new DummyResource(0,"Tren"));
        arr.add(new DummyResource(0,"Camioneta"));
        arr.add(new DummyResource(0,"Van"));
        arr.add(new DummyResource(0,"Minivan"));
        arr.add(new DummyResource(0,"Combi"));
        arr.add(new DummyResource(0,"Combi"));
        arr.add(new DummyResource(0,"Bicicleta"));
        arr.add(new DummyResource(0,"Moto"));
        arr.add(new DummyResource(0,"Motocicleta"));
        arr.add(new DummyResource(0,"Triciclo"));
        arr.add(new DummyResource(0,"Barco"));
        arr.add(new DummyResource(0,"Yate"));
        titles.add(new DummyGroup("Medios de ransporte",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Iremos de compras con el auto"));
        arr.add(new DummyResource(0,"Yo y mis amigos iremos a pasear en bicicleta"));
        arr.add(new DummyResource(0,"Nuestro equipo de ganadores irán de paseo en un bus privado"));
        arr.add(new DummyResource(0,"Saldré a pasear a mi perro"));
        arr.add(new DummyResource(0,"Saldré a pasear a mi perro, nos vemos más rato"));
        arr.add(new DummyResource(0,"Saldré a pasear a mi perro, nos vemos en media hora"));
        arr.add(new DummyResource(0,"Saldré a pasear a mi perro, nos vemos mañana"));
        arr.add(new DummyResource(0,"Saldré a pasear a mi perro, nos vemos en tu casa"));
        arr.add(new DummyResource(0,"Saldré a pasear con mis hijos"));
        arr.add(new DummyResource(0,"Saldré a pasear con mi novia"));
        arr.add(new DummyResource(0,"Creo que daré un paseo por el parque"));
        arr.add(new DummyResource(0,"Creo que daré un paseo por el parque de diversiones"));
        arr.add(new DummyResource(0,"Gordito debemos dar un paseo por el mar"));
        arr.add(new DummyResource(0,"Daré un paseo en avioneta por las líneas de nazca"));
        arr.add(new DummyResource(0,"Hoy inicia el paseo por toda la ciudad blanca de Arequipa"));
        titles.add(new DummyGroup("Paseo",arr));


        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Viaje"));
        arr.add(new DummyResource(0,"Prefiero viajar en primera clase"));
        arr.add(new DummyResource(0,"Prefiero viajar en clase económica"));
        arr.add(new DummyResource(0,"Necesito realizar un viaje a España"));
        arr.add(new DummyResource(0,"Necesito realizar un viaje a Perú"));
        arr.add(new DummyResource(0,"Me gustaría viajar a Machu Picchu"));
        arr.add(new DummyResource(0,"Creo que viajaré hacia el nevado Huascarán"));
        arr.add(new DummyResource(0,"La próxima semana viajaré al Huascarán"));
        arr.add(new DummyResource(0,"La próxima semana viajaré al nevado Huascarán"));
        arr.add(new DummyResource(0,"Gordito debemos viajar a la línea ecuatorial"));
        arr.add(new DummyResource(0,"Me siento estresado, creo que es el momento de viajar y conocer el mundo"));
        arr.add(new DummyResource(0,"Ese viaje fue espectacular"));
        arr.add(new DummyResource(0,"Ese viaje me hizo sentir bien"));
        arr.add(new DummyResource(0,"Ese viaje fue buenísimo para mi salud"));
        arr.add(new DummyResource(0,"No hay nada mejor que viajar en familia"));
        arr.add(new DummyResource(0,"No hay nada mejor que viajar acompañado y vivir experiencias únicas"));
        arr.add(new DummyResource(0,"Cuántas horas demora viajar desde España hacia Perú ?"));
        arr.add(new DummyResource(0,"Viajar en bicicleta por toda sudamérica debe ser increíble"));
        arr.add(new DummyResource(0,"Manejar este vehículo parece un viaje internacional porque demora mucho"));
        arr.add(new DummyResource(0,"El viaje en bus hizo que me mareara bien feo"));
        arr.add(new DummyResource(0,"El viaje en bus hizo que me dé un mareo único"));
        arr.add(new DummyResource(0,"Estoy mareado con este viaje"));
        arr.add(new DummyResource(0,"¿ Este barco tiene dormitorios ?"));
        arr.add(new DummyResource(0,"Casi siempre viajo en fiestas navideñas"));
        arr.add(new DummyResource(0,"Erick viajó en su cumpleaños"));
        titles.add(new DummyGroup("Viaje",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Tren"));
        arr.add(new DummyResource(0,"Este tren es super rápido"));
        arr.add(new DummyResource(0,"¿ Este tren es el tren bala ?"));
        arr.add(new DummyResource(0,"Espero que esté bien sentado en este tren"));
        arr.add(new DummyResource(0,"Éste tren es super cómodo"));
        arr.add(new DummyResource(0,"¿ En dónde se encuentra la estación más cercana de este tren ?"));
        arr.add(new DummyResource(0,"¿ En dónde se encuentra la estación más cercana del tren ?"));
        arr.add(new DummyResource(0,"¿ Cuánto demorará el tren en llegar al próximo paradero ?"));
        arr.add(new DummyResource(0,"¿ Cuánto cuesta el pasaje en este tren ?"));
        arr.add(new DummyResource(0,"¿ Cuál será el tren más largo del mundo ?"));
        arr.add(new DummyResource(0,"¿ Hasta qué hora atiende el tren ?"));
        arr.add(new DummyResource(0,"Voy a comprarme un tren eléctrico"));
        arr.add(new DummyResource(0,"No tengo saldo para viajar en el tren"));
        arr.add(new DummyResource(0,"No cuento con crédito para viajar en el tren"));
        arr.add(new DummyResource(0,"¿ En qué dirección viaja el tren ?"));
        arr.add(new DummyResource(0,"Está demorando mucho el viaje en el tren"));
        arr.add(new DummyResource(0,"Papá cómprame este tren a control remoto"));
        titles.add(new DummyGroup("Tren",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Bicicleta"));
        arr.add(new DummyResource(0,"Tengo la mejor bicicleta del mundo"));
        arr.add(new DummyResource(0,"Tengo la mejor bicicleta que todos ustedes"));
        arr.add(new DummyResource(0,"Se malogró mi bicicleta"));
        arr.add(new DummyResource(0,"Con esta bicicleta ganaré la carrera"));
        arr.add(new DummyResource(0,"Iré a pasear en bicicleta con mi enamorada"));
        arr.add(new DummyResource(0,"Me regalarán una bicileta en mi cumpleaños"));
        arr.add(new DummyResource(0,"Tengo una bicicleta que me ha acompañado en todo momento"));
        arr.add(new DummyResource(0,"Te vendo mi bicicleta"));
        arr.add(new DummyResource(0,"A tu bicicleta le falta aceite en su cadena"));
        arr.add(new DummyResource(0,"Se malogró la cadena de la bicicleta"));
        arr.add(new DummyResource(0,"El gordito rompió la bicicleta"));
        arr.add(new DummyResource(0,"El gordito pesa mucho para la bicicleta"));
        arr.add(new DummyResource(0,"Iré al colegio en bicicleta"));
        arr.add(new DummyResource(0,"¿ Donde puedo inflar la llanta de mi bicicleta ?"));
        arr.add(new DummyResource(0,"Estoy pensando en importar 50 bicicletas"));
        titles.add(new DummyGroup("Bicicleta",arr));

        arr = new ArrayList<>();
        arr.add(new DummyResource(0,"Auto"));
        arr.add(new DummyResource(0,"Este auto es muy lindo"));
        arr.add(new DummyResource(0,"Estar en este auto se siente cómodo"));
        arr.add(new DummyResource(0,"Iré al trabajo en auto"));
        arr.add(new DummyResource(0,"Se malogró la llanta del auto"));
        arr.add(new DummyResource(0,"Tengo pensado comprarme un auto"));
        arr.add(new DummyResource(0,"Tengo pensado adquirir un auto cero kilómetros"));
        titles.add(new DummyGroup("Auto",arr));


        DummyContent.fill();
    }
    public static void fillTemplate() {
        titles.clear();
        ArrayList<DummyResource> arr = new ArrayList<>();
        arr.add(new DummyResource(0,"sample"));
        titles.add(new DummyGroup("sample",arr));

        DummyContent.fill();
    }

    private static void fill(){
        ITEMS.clear();
        ITEM_MAP.clear();
        for (int i = 1; i <= titles.size(); i++) {
            addItem(createDummyItem(i,titles.get(i-1)));
        }
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static DummyItem createDummyItem(int position,DummyGroup dummyGroup) {
        return new DummyItem(String.valueOf(position), dummyGroup.title,dummyGroup.title,dummyGroup);
    }


    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String id;
        public final String content;
        public final String details;
        public final DummyGroup dummyGroup;

        public DummyItem(String id, String content, String details, DummyGroup dummyGroup) {
            this.id = id;
            this.content = content;
            this.details = details;
            this.dummyGroup = dummyGroup;
        }

        @Override
        public String toString() {
            return content;
        }

    }
    public static class DummyResource {
        public final int resource_id;
        public final String text;

        public DummyResource(int resource_id, String text) {
            this.resource_id = resource_id;
            this.text = text;
        }
    }
    public static class DummyGroup{
        public final String title;
        public final ArrayList<DummyResource> resources;

        public DummyGroup(String title, ArrayList<DummyResource> resources) {
            this.title = title;
            this.resources = resources;
        }
    }
}
