package com.arquigames.aprendiendoespanol.items;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;

import com.arquigames.aprendiendoespanol.R;
import com.arquigames.aprendiendoespanol.items.dummy.DummyContent;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import static com.arquigames.aprendiendoespanol.items.ItemElementsDetailFragment.ARG_ITEM_ID;
import static com.arquigames.aprendiendoespanol.items.ItemElementsDetailFragment.ARG_TITLE;

/**
 * An activity representing a single ItemSaludo detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link ItemElementsListActivity}.
 */
public class ItemElementsDetailActivity extends AppCompatActivity {

    private static final String TAG = "ItemSalDetailActivity";
    private DummyContent.DummyItem mItem;
    private String mTitle;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itemsaludo_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(ARG_ITEM_ID,
                    getIntent().getStringExtra(ARG_ITEM_ID));
            arguments.putString(ARG_TITLE,
                    getIntent().getStringExtra(ARG_TITLE));
            ItemElementsDetailFragment fragment = new ItemElementsDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.itemsaludo_detail_container, fragment)
                    .commit();
        }

        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            //NavUtils.navigateUpTo(this, new Intent(this, ItemElementsListActivity.class));
            //return true;
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    public static void setupRecyclerView(@NonNull RecyclerView recyclerView,DummyContent.DummyItem item,Context ctx) {
        Log.e(TAG,">>>>>>>>>>>>>>setupRecyclerView");
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(item,ctx));
    }

    public static class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<DummyContent.DummyResource> mResources;
        private Context mCtx;
        private MediaPlayer mediaPlayer;

        SimpleItemRecyclerViewAdapter(DummyContent.DummyItem item, Context ctx) {
            mResources = item.dummyGroup.resources;
            mCtx = ctx;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.itemsaludo_detail_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            //holder.mResourceView.setText(String.format(Locale.US,"%03d",mResources.get(position).resource_id));
            DummyContent.DummyResource res = mResources.get(position);
            holder.mContentView.setText(res.text);

            holder.itemView.setTag(res);
            View imageButton = holder.itemView.findViewById(R.id.imageButton);
            imageButton.setTag(res);
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DummyContent.DummyResource resource = (DummyContent.DummyResource) v.getTag();
                    if( resource.resource_id > 0 ){
                        if(mediaPlayer!=null && mediaPlayer.isPlaying()){
                            mediaPlayer.stop();
                            mediaPlayer.release();
                            mediaPlayer = null;
                        }
                        mediaPlayer = MediaPlayer.create(mCtx, resource.resource_id);
                        mediaPlayer.start();
                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                                mediaPlayer = null;
                                mp = null;
                            }
                        });
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mResources.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            //final TextView mResourceView;
            final TextView mContentView;

            ViewHolder(View view) {
                super(view);
                //mResourceView = view.findViewById(R.id.resource);
                mContentView = view.findViewById(R.id.content);
            }
        }
    }
}
